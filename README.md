# messenger-arch

![Messenger Architecture](./diagrams/d2-code.png)

- The architecture consists of key components like the client application, web server, message server, database server, file storage, notification service.
- The client application communicates with the web server over HTTP/HTTPS for user interactions.
- The web server handles client requests and communicates with the message server using WebSocket for real-time messaging.
- The message server interacts with the database server for CRUD (Create, Read, Update, Delete) operations on user data, channel data, and message data.
- The message server also communicates with the file storage system for uploading and downloading files.
- Push notifications are handled by the notification service.

P.S. This project is intended to demonstrate drawing of architectural diagrams within the CI pipeline, followed by the addition of images to README.md.
